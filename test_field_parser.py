import pytest

import field_parser


@pytest.mark.parametrize("bad_string", ["60-70", "-", "/", "", "2"])
def test__generic_parser__with_erroneous_input__raises_assertion_error(bad_string):
    with pytest.raises(AssertionError):
        p = field_parser.CronFieldParser(bad_string, "", (0, 1))
        p.expand()


@pytest.mark.parametrize("day_range,expected_output", [
    ("SUN", "0"),
    ("MON", "1"),
    ("TUE", "2"),
    ("WED", "3"),
    ("THU", "4"),
    ("FRI", "5"),
    ("SAT", "6"),
    ("MON-FRI", "1 2 3 4 5"),
    ("SAT,SUN", "0 6"),
    ("*/2", "0 2 4 6"),
    ("MON-THU/2", "1 3"),
])
def test__day_of_week_parser__with_string_input__expands_to_int(day_range, expected_output):
    p = field_parser.Cron_DayOfWeek(day_range)
    assert p.expand() == expected_output


@pytest.mark.parametrize("month_range,expected_output", [
    ("JAN", "1"),
    ("FEB", "2"),
    ("MAR", "3"),
    ("APR", "4"),
    ("MAY", "5"),
    ("JUN", "6"),
    ("JUL", "7"),
    ("AUG", "8"),
    ("SEP", "9"),
    ("OCT", "10"),
    ("NOV", "11"),
    ("DEC", "12"),
    ("JAN-DEC", "1 2 3 4 5 6 7 8 9 10 11 12"),
    ("APR,AUG,DEC", "4 8 12"),
    ("JAN-DEC/4", "1 5 9"),
])
def test__day_of_week_parser__with_string_input__expands_to_int(month_range, expected_output):
    p = field_parser.Cron_Month(month_range)
    assert p.expand() == expected_output
