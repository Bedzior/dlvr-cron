```
   (@@@@@@@@@@@@@@  @@@@@@@@@@#                @@@@             %
 ,@@@@@@@@@@@@@@&  @@@@@@@@@@@@@@@@      /@@@@@@@@@@@@@@#   %@@@@@@@      &@@@@@
 @@@@@             @@@@@      &@@@@@   @@@@@@@      #@@@@@* %@@@@@@@@@@   @@@@@(
@@@@@(             %@@@@ @@@@@@@@&    /@@@@@.        ,@@@@@  @@@@@@@@@@@@@@@@@@
 @@@@@@@@@###      %@@@@  @@@@@#       @@@@@@@%    #@@@@@@.  @@@@%  *@@@@@@@@@@
    @@@@@@@@@@@    @@@@@   &@@@@@@        @@@@@@@@@@@@@&    %@@@@%     *@@@@@@@
                             (@@@@@@,                       /             (&#
                                (@@@@@@&

    thanks to https://manytools.org/hacker-tools/convert-images-to-ascii-art/
```


# Cron Expression Parser #
> [...] a command line application or script which parses a cron string and expands each field to show the times at which it will run.

Written and tested in Python.

## Setup ##
Make sure to have at least Python version 3.8 installed.
The project has not been tested with prior nor newer versions, but should run just as smoothly.

## Usage ##
Pass a quoted cron string as the sole parameter to the script:
```bash
./parser.py ＂*/15 0 1,15 * 1-5 /usr/bin/find＂
```

which should in turn yield the following output:
```p
minute         0 15 30 45
hour           0
day of month   1 15
month          1 2 3 4 5 6 7 8 9 10 11 12
day of week    1 2 3 4 5
command        /usr/bin/find
```

## Testing ##
To enable testing, remember to install `pytest`:
```bash
pip install pytest
```

Running the tests is as simple as calling `pytest` in this project's root directory:
```bash
pytest .
```

## Contribution guide ##
Maintain code readability.
Keep the tests up to date (CI is always watching).
Keep the codebase clean with `autopep8`.

## Improvement ideas ##
1. Assertions are obviously misused, acting as stand-in for proper error types.
1. Error could carry position information, to pinpoint the exact location of expression issue.
1. Sunday special case with both 0 and 7 as accepted values could be implemented.
1. &hellip;
