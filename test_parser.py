import pytest

from parser import CronExpression


@pytest.mark.parametrize(
    "cron_string,expected_output",
    [("""
*/15 0 1,15 * 1-5 /usr/bin/find
""", """
minute         0 15 30 45
hour           0
day of month   1 15
month          1 2 3 4 5 6 7 8 9 10 11 12
day of week    1 2 3 4 5
command        /usr/bin/find
""")]
)
def test__example_from_docs__gives_expected_output(cron_string, expected_output):
    assert CronExpression.from_string(
        cron_string.strip()).tabulate() == expected_output.strip()


@pytest.mark.parametrize(
    "cron_string,expected_output",
    [("""
* * * * * /usr/bin/find
""", """
minute         0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59
hour           0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
day of month   1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
month          1 2 3 4 5 6 7 8 9 10 11 12
day of week    0 1 2 3 4 5 6
command        /usr/bin/find
""")]
)
def test__all_asterisks__gives_lengthy_output(cron_string, expected_output):
    assert CronExpression.from_string(
        cron_string.strip()).tabulate() == expected_output.strip()
