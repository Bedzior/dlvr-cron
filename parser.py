#!/usr/bin/env python3

import sys

from dataclasses import dataclass, fields

from field_parser import *


class CronExpressionParser:
    @staticmethod
    def parse(string: str):
        arr = string.split(' ')
        assert len(arr) >= 6, f"Expecting 6 elements in cron expression, got {len(arr)}"
        minute = Cron_Minute(arr[0])
        hour = Cron_Hour(arr[1])
        day_of_month = Cron_DayOfMonth(arr[2])
        month = Cron_Month(arr[3])
        day_of_week = Cron_DayOfWeek(arr[4])
        command = Cron_Command(' '.join(arr[5:]))

        return minute, hour, day_of_month, month, day_of_week, command


@dataclass(eq=False)
class CronExpression:
    minute: Cron_Minute
    hour: Cron_Hour
    day_of_month: Cron_DayOfMonth
    month: Cron_Month
    day_of_week: Cron_DayOfWeek
    command: Cron_Command

    @classmethod
    def from_string(cls, expression: str):
        return CronExpression(*CronExpressionParser.parse(expression))

    @property
    def fields(self):
        return self.minute, self.hour, self.day_of_month, self.month, self.day_of_week, self.command

    def tabulate(self) -> str:
        longest = max([len(f.header) for f in self.fields])

        rows = []
        for field in self.fields:
            rows.append(f"{field.header.ljust(longest)}   {field.expand()}")
        return '\n'.join(rows)


def main():
    assert len(sys.argv) == 2, "Expecting a single, quoted argument"
    args = sys.argv[1]

    print(CronExpression.from_string(args).tabulate())


if __name__ == "__main__":
    main()
