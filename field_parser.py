from typing import Final, Tuple


class CronFieldParser:
    RANGE_DELIMITER: Final = '-'
    VALUE_LIST_SEPARATOR: Final = ','
    STEP_MARKER: Final = '/'
    ANY_VALUE_MARKER: Final = '*'

    def __init__(self, expression: str, header: str, bounds: Tuple[int, int]):
        '''Full expression string provided for a field'''
        self.expression = expression
        '''Table header'''
        self.header = header
        '''Bounds for field values (inclusive from both ends)'''
        assert bounds[0] <= bounds[1], "Range start greater than stop"
        self.bounds = bounds

    def map_value(self, v):
        return v

    def _parse_range(self, str: str) -> Tuple[int, int]:
        if str == CronFieldParser.ANY_VALUE_MARKER:
            bounds = self.bounds
        elif CronFieldParser.RANGE_DELIMITER in str:
            ar = str.split(CronFieldParser.RANGE_DELIMITER)
            assert len(
                ar) == 2, f"Expecting single range delimiter ({CronFieldParser.RANGE_DELIMITER}) in range expression"
            try:
                bounds = tuple(map(int, map(self.map_value, ar)))
            except:
                assert False, "Non-numeric input in range"
        else:
            assert len(str), "No value in field"
            try:
                bounds = int(self.map_value(str)), int(self.map_value(str))
            except:
                assert False, "Non-numeric input in field"
        assert bounds[0] <= bounds[1], "Range start greater than stop"
        assert bounds[0] >= self.bounds[0] and bounds[1] <= self.bounds[1], "Range out of bounds for current field"
        return bounds[0], bounds[1] + 1

    def expand(self) -> str:
        expanded_expression = []
        if CronFieldParser.VALUE_LIST_SEPARATOR in self.expression:
            value_lists = self.expression.split(
                CronFieldParser.VALUE_LIST_SEPARATOR)
        else:
            value_lists = [self.expression]
        for value_list in value_lists:
            if CronFieldParser.STEP_MARKER in value_list:
                arr = value_list.split(CronFieldParser.STEP_MARKER)
                assert len(
                    arr) == 2, f"Expecting single step marker ({CronFieldParser.STEP_MARKER}) in value list expression"
                assert len(arr[1])
                step = int(arr[1])
                bounds = self._parse_range(arr[0])
            else:
                step = 1
                bounds = self._parse_range(value_list)

            expanded_expression.extend(range(*bounds, step))
        # remove duplicates the quickest way
        expanded_expression = list(set(expanded_expression))
        expanded_expression.sort()
        return ' '.join(map(str, expanded_expression))


class Cron_Minute(CronFieldParser):
    def __init__(self, expression: str):
        super().__init__(expression, "minute", (0, 59))


class Cron_Hour(CronFieldParser):
    def __init__(self, expression: str):
        super().__init__(expression, "hour", (0, 23))


class Cron_DayOfMonth(CronFieldParser):
    def __init__(self, expression: str):
        super().__init__(expression, "day of month", (1, 31))


class Cron_Month(CronFieldParser):
    def __init__(self, expression: str):
        super().__init__(expression, "month", (1, 12))

    def map_value(self, month):
        try:
            return int(month)
        except:
            mapping = {
                'JAN': 1,
                'FEB': 2,
                'MAR': 3,
                'APR': 4,
                'MAY': 5,
                'JUN': 6,
                'JUL': 7,
                'AUG': 8,
                'SEP': 9,
                'OCT': 10,
                'NOV': 11,
                'DEC': 12,
            }
            assert month in mapping
            return mapping[month]


class Cron_DayOfWeek(CronFieldParser):
    def __init__(self, expression: str):
        super().__init__(expression, "day of week", (0, 6))

    def map_value(self, day):
        try:
            return int(day)
        except:
            mapping = {
                'SUN': 0,
                'MON': 1,
                'TUE': 2,
                'WED': 3,
                'THU': 4,
                'FRI': 5,
                'SAT': 6,
            }
            assert day in mapping
            return mapping[day]


class Cron_Command(CronFieldParser):
    def __init__(self, expression: str):
        self.expression = expression
        self.header = "command"

    def expand(self):
        return self.expression
